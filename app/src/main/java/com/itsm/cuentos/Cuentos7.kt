package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Cuentos7 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos7)
        verCuento()
    }

    private fun verCuento() {
        var buttonView: Button = findViewById(R.id.button_view7)

        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story7)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "$name_person– Señora, este es el mejor arroz con pollo que he comido en " +
                    "toda mi vida ¡Tiene usted manos de oro para la cocina!\n" +
                    "\n" +
                    "– ¡Muchas gracias, me alegro mucho de que le guste! ¿Le apetece un café " +
                    "con bizcocho de manteca?\n" +
                    "\n" +
                    "$name_person– Si no es molestia, acepto encantado su invitación.\n" +
                    "\n" +
                    "– ¡Claro que no, ahora mismo se lo traigo!"
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos8::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}