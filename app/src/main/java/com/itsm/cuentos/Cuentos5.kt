package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Cuentos5 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos5)
        verCuento()
    }

    private fun verCuento() {
        var buttonView: Button = findViewById(R.id.button_view5)

        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story5)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "– ¿En serio?… Pues entonces no podemos dejarle escapar " +
                    "¡Tenemos que aprovecharnos de $name_person como sea!\n" +
                    "\n" +
                    "– ¡Sí! Vamos a intentar que esté lo más contento posible y ya se me ocurrirá algo."
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos6::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}