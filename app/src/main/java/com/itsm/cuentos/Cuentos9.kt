package com.itsm.cuentos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Cuentos9 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos9)
        var text_content: TextView = findViewById(R.id.text_story9)
        val bundle: Bundle? = intent.extras
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "El astuto $name_person salió de la casa con la intención de no " +
                    "volver. Afuera, junto a las escaleras de la entrada, seguía jugando el " +
                    "niño; parecía muy entretenido haciendo un avión de papel con el " +
                    "billete que un par de horas antes le había regalado. Se acercó a " +
                    "él y de un tirón, se lo quitó.\n" +
                    "\n" +
                    "$name_person– ¡Dame ese billete, chaval, que ya has jugado bastante!\n" +
                    "\n" +
                    "Lo guardó en el bolsillo, rodeó la casa y $name_person echó a correr.."
        }
    }
}