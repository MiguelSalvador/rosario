package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Cuentos4 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos4)
        verCuento()
    }

    private fun verCuento() {
        var buttonView: Button = findViewById(R.id.button_view4)

        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story4)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "Aguantando la risa, $name_person pasó al comedor y se sentó " +
                    "a la mesa ¡$name_person había echado el anzuelo y el pez había picado!\n" +
                    "\n" +
                    "Mientras, el granjero, un poco nervioso, entró en la cocina para " +
                    "hablar con su mujer. En voz baja, le dijo:\n" +
                    "\n" +
                    "– Creo que este desconocido está forrado de dinero porque le ha regalado " +
                    "a nuestro hijo un billete de cinco pesos  ¡y le escuché decir que tiene muchos más!"
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos5::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}