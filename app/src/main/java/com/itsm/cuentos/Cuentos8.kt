package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Cuentos8 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos8)
        verCuento()
    }

    private fun verCuento() {

        var buttonView: Button = findViewById(R.id.button_view8)
        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story8)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "$name_person-El postre estaba para chuparse los dedos y el humeante café " +
                    "fue el colofón perfecto a una comida espectacular.\n" +
                    "\n" +
                    "$name_person– Muchas gracias, señores, todo estaba  realmente delicioso. Y ahora " +
                    "si me disculpan, necesito ir al servicio… ¿Podrían indicarme dónde está?\n" +
                    "\n" +
                    "– ¡Claro, faltaría más! El retrete está junto al granero; salga que en seguida lo verá.\n" +
                    "\n" +
                    "$name_person– Muchas gracias, caballero, ahora mismo vuelvo."
        }
        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos9::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}