package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Cuentos3 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos3)
        verCuento()
    }

    private fun verCuento() {
        var buttonView: Button = findViewById(R.id.button_view3)

        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story3)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "$name_person– ¡Toma, guapo, para que juegues! ¡Si quieres otro dímelo, que tengo muchos de estos!\n" +
                    "\n" +
                    "El granjero vio de reojo cómo $name_person le regalaba un billete de los gordos a su hijo y pensó:\n" +
                    "\n" +
                    "– “Este tipo debe ser rico y eso cambia las cosas… ¡Le invitaré a entrar!”\n" +
                    "\n" +
                    "Abrió la puerta de nuevo y con una gran sonrisa en la cara, le dijo muy educadamente:\n" +
                    "\n" +
                    "– ¡Está bien, pase! Mi mujer le preparará algo bueno que llevarse a la boca.\n" +
                    "\n" +
                    "$name_person– ¡Oh, es usted muy amable, gracias!"
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos4::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}