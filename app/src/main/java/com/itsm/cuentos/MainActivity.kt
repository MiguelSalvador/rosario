package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var buttonGegenerate: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonGegenerate = findViewById(R.id.button_cuento)
        generateStory()
    }

    private fun generateStory() {
        buttonGegenerate.setOnClickListener{
            var nameText: EditText = findViewById(R.id.name_box)
            var name = nameText.text.toString()
            if(name.isEmpty()){
                Toast.makeText(this, "Debe ingresar un nombre primero", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val bundle = Bundle()
            bundle.apply {
                putString("key_name",name)
            }

            val intent = Intent(this, Story::class.java).apply(){
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }
}