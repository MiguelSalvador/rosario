package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Story : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story)
        verCuento()
    }

    private fun verCuento() {
        var buttonView: Button = findViewById(R.id.button_view)

        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "Tras varias horas caminando bajo el sol $name_person pasó por una " +
                    "pequeña granja, la única que había en muchos kilómetros a la redonda. " +
                    "El olorcillo a cocido llegó hasta su nariz y $name_person se dio cuenta de que tenía " +
                    "un hambre de lobo. $name_person llamó a la puerta y el dueño de la casa, bastante " +
                    "antipático, le abrió. – Buenas tardes, señor.– ¿Quién es usted y qué busca " +
                    "por estos lugares?– No se asuste, soy un simple viajero que va de paso. " +
                    "Me preguntaba si podría invitarme a un plato de comida. Estoy muerto de " +
                    "hambre y no hay por aquí ninguna posada donde tomar algo caliente."
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos2::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}