package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Cuentos2 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos2)
        verCuento()
    }

    private fun verCuento() {
        var buttonView: Button = findViewById(R.id.button_view2)

        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story2)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "El granjero no se compadeció y para quitárselo de encima le " +
                    "dijo en un tono muy despectivo:\n" +
                    "\n" +
                    "– ¡Pues no, no puedo! Son las cinco y mi esposa y yo ya hemos comido " +
                    "¡En esta casa somos muy puntuales y estrictos con los horarios, así que no voy a hacer ninguna excepción! ¡Váyase por donde vino!\n" +
                    "\n" +
                    "$name_person se quedó chafado, pero en vez de venirse abajo, reaccionó " +
                    "con astucia; justo cuando el granjero iba a darle con la puerta en las " +
                    "narices, $name_person sacó un billete de cinco pesos del bolsillo de su pantalón y se " +
                    "lo dio a un niño que jugaba en la entrada."
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos3::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}