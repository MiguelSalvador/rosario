package com.itsm.cuentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Cuentos6 : AppCompatActivity() {
    var name_person: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuentos6)
        verCuento()
    }

    private fun verCuento() {
        var buttonView: Button = findViewById(R.id.button_view6)

        val bundle: Bundle? = intent.extras
        var text_content: TextView = findViewById(R.id.text_story6)
        bundle?.let{
            name_person = it.getString("key_name")
            text_content.text = "El granjero y su mujer adornaron la mesa con flores y " +
                    "sirvieron la comida en platos de porcelana fina que se sintiera como " +
                    "un rey, pero $name_person sabía que tanta atención no era ni por " +
                    "caridad ni por amabilidad, sino que lo hacían por puro interés, " +
                    "porque pensaban que era rico y querían quedarse con parte de su " +
                    "dinero ¡El plan de $name_person había surtido efecto porque era lo que él quería que pensaran!"
        }

        var bundle2 = Bundle()
        bundle2.apply{ putString("key_name",name_person) }

        buttonView.setOnClickListener{
            var intent = Intent(this,Cuentos7::class.java).apply {
                putExtras(bundle2)
            }
            startActivity(intent)
        }
    }
}